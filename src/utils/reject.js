import Vue from "vue";

let reject = response => {
  let error = (response && response.body && response.body.message) || ("string" === typeof response ? response : "An error occured");
  if (401 !== response.status) {
    Vue.modal.msgBoxOk(error, { title: "Error" });
  }
  return Promise.reject(error);
};

export default reject;
