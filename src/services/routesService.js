import Vue from "vue";
import graphlib from "graphlib";

let dirs = {
  "1,1": "↘",
  "1,0": "↓",
  "1,-1": "↙",
  "0,-1": "←",
  "-1,-1": "↖",
  "-1,0": "↑",
  "-1,1": "↗",
  "0,1": "→"
};

let findRoute = (world, way, graph, type) => {
  let start = way.start.x + "," + way.start.y;
  let end = way.end.x + "," + way.end.y;
  let m = graphlib.alg.dijkstra(
    graph,
    start,
    e => graph.edge(e),
    v => graph.nodeEdges(v)
  );

  if (!m[end] || Infinity === m[end].distance) {
    return { path: [], distance: "∞" };
  }

  let node = end;
  let path = [node];
  do {
    let coords = node.split(",");
    let predecessor = m[node].predecessor;
    let predecessorCoords = predecessor.split(",");
    world.cells[predecessorCoords[0]][predecessorCoords[1]][type] =
      dirs[
        coords[0] -
          predecessorCoords[0] +
          "," +
          (coords[1] - predecessorCoords[1])
      ];
    node = predecessor;
    path.push(node);
  } while (node !== start);
  let distance = m[end].distance;
  return {
    path: path,
    distance: isNaN(distance) ? distance : distance.toFixed(2)
  };
};

let getGroundRoute = (world, way) => {
  let type = "GROUND";

  let directions = [
    { name: "←", value: { x: 0, y: -1 } },
    { name: "↑", value: { x: -1, y: 0 } },
    { name: "→", value: { x: 0, y: 1 } },
    { name: "↓", value: { x: 1, y: 0 } }
  ];

  let graph = new graphlib.Graph({
    directed: false,
    compound: false,
    multigraph: false
  });
  world.cells.forEach((row, i) => {
    row.forEach((cell, j) => {
      delete cell[type];
      switch (cell.type) {
        case "BUILDING":
        case "TREE":
        case "WATER":
          return;
        default:
          graph.setNode(i + "," + j, cell);
      }
    });
  });

  graph.nodes().forEach(node => {
    let cell = graph.node(node);
    let i = cell.x;
    let j = cell.y;
    directions.forEach(direction => {
      let u = i + direction.value.x;
      if (u < 0 || u >= world.measure) return;
      let v = j + direction.value.y;
      if (v < 0 || v >= world.measure) return;
      let neighbor = world.cells[u][v];
      switch (neighbor.type) {
        case "BUILDING":
        case "TREE":
        case "WATER":
          return;
        default:
          graph.setEdge(node, u + "," + v, 1);
      }
    });
  });

  return findRoute(world, way, graph, type);
};

let getAirRoute = (world, way) => {
  let type = "AIR";

  let directions = [
    { name: "←", value: { y: -1, x: 0 } },
    { name: "↖", value: { y: -1, x: -1 } },
    { name: "↑", value: { y: 0, x: -1 } },
    { name: "↗", value: { y: 1, x: -1 } },
    { name: "→", value: { y: 1, x: 0 } },
    { name: "↘", value: { y: 1, x: 1 } },
    { name: "↓", value: { y: 0, x: 1 } },
    { name: "↙", value: { y: -1, x: 1 } }
  ];

  let graph = new graphlib.Graph({
    directed: false,
    compound: false,
    multigraph: false
  });
  world.cells.forEach((row, i) => {
    row.forEach((cell, j) => {
      delete cell[type];
      graph.setNode(i + "," + j, cell);
    });
  });
  graph.nodes().forEach(node => {
    let cell = graph.node(node);
    let i = cell.x;
    let j = cell.y;
    directions.forEach(direction => {
      let u = i + direction.value.x;
      if (u < 0 || u >= world.measure) return;
      let v = j + direction.value.y;
      if (v < 0 || v >= world.measure) return;
      graph.setEdge(
        node,
        u + "," + v,
        Math.abs(u - i) + Math.abs(v - j) > 1 ? 1.41 : 1
      );
    });
  });

  return findRoute(world, way, graph, type);
};

let getWaterRoute = (world, way) => {
  let type = "WATER";
  let directions = [
    { name: "←", value: { y: -1, x: 0 } },
    { name: "↖", value: { y: -1, x: -1 } },
    { name: "↑", value: { y: 0, x: -1 } },
    { name: "↗", value: { y: 1, x: -1 } },
    { name: "→", value: { y: 1, x: 0 } },
    { name: "↘", value: { y: 1, x: 1 } },
    { name: "↓", value: { y: 0, x: 1 } },
    { name: "↙", value: { y: -1, x: 1 } }
  ];

  let graph = new graphlib.Graph({
    directed: false,
    compound: false,
    multigraph: false
  });
  world.cells.forEach((row, i) => {
    row.forEach((cell, j) => {
      delete cell[type];
      if ("WATER" !== cell.type) return;
      graph.setNode(i + "," + j, cell);
    });
  });
  graph.nodes().forEach(node => {
    let cell = graph.node(node);
    let i = cell.x;
    let j = cell.y;
    directions.forEach(direction => {
      let u = i + direction.value.x;
      if (u < 0 || u >= world.measure) return;
      let v = j + direction.value.y;
      if (v < 0 || v >= world.measure) return;

      let neighbor = world.cells[u][v];
      switch (neighbor.type) {
        case "BUILDING":
        case "TREE":
        case "DESERT":
          return;
        default:
          graph.setEdge(node, u + "," + v, 1);
      }

      graph.setEdge(
        node,
        u + "," + v,
        Math.abs(u - i) + Math.abs(v - j) > 1 ? 1.41 : 1
      );
    });
  });

  return findRoute(world, way, graph, type);
};

let getUndergroundRoute = (world, way) => {
  let type = "UNDERGROUND";
  let directions = [
    { name: "←", value: { y: -1, x: 0 } },
    { name: "↑", value: { y: 0, x: -1 } },
    { name: "→", value: { y: 1, x: 0 } },
    { name: "↓", value: { y: 0, x: 1 } }
  ];

  let graph = new graphlib.Graph({
    directed: false,
    compound: false,
    multigraph: false
  });
  world.cells.forEach((row, i) => {
    row.forEach((cell, j) => {
      delete cell[type];
      if (!cell.underground) return;
      graph.setNode(i + "," + j, cell);
    });
  });
  graph.nodes().forEach(node => {
    let cell = graph.node(node);
    let i = cell.x;
    let j = cell.y;
    directions.forEach(direction => {
      let u = i + direction.value.x;
      if (u < 0 || u >= world.measure) return;
      let v = j + direction.value.y;
      if (v < 0 || v >= world.measure) return;
      let neighbor = world.cells[u][v];
      if (neighbor.underground) {
        graph.setEdge(
          node,
          u + "," + v,
          Math.abs(u - i) + Math.abs(v - j) > 1 ? 1.41 : 1
        );
      }
    });
  });
  return findRoute(world, way, graph, type);
};

let RoutesService = {
  userWaytypes(world) {
    let waytypeChoices = Vue.store.getters.user.settings.waytypeChoices;
    let vehicles = world.vehicles;
    return vehicles.reduce((o, v /*, k*/) => {
      let wayType = v.wayType;
      let choice = waytypeChoices[wayType];
      if (choice && choice.selected && !o.includes(wayType)) {
        o.push(wayType);
      }
      return o;
    }, []);
  },
  vehicleAvailable(vehicle) {
    let waytypeChoices = Vue.store.getters.user.settings.waytypeChoices;
    let choice = waytypeChoices[vehicle.wayType];
    return choice && choice.selected;
  },
  getRoute(world, way, wayType) {
    switch (wayType) {
      case "AIR":
        return getAirRoute(world, way);
      case "GROUND":
        return getGroundRoute(world, way);
      case "WATER":
        return getWaterRoute(world, way);
      case "UNDERGROUND":
        return getUndergroundRoute(world, way);
    }
  }
};

export default RoutesService;
