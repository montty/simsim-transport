import Vue from "vue";
import reject from "@/utils/reject";

let SignService = {
  getUserData() {
    return Vue.http.get().then(response => {
      let user = response.body;
      Vue.store.commit("setUserData", user);
      return user;
    }, reject);
  },
  login(login) {
    return Vue.http
      .post("login?login=" + encodeURIComponent(login))
      .then(() => {
        Vue.router.push({ name: "password-form" });
      }, reject);
  },
  password(password) {
    return Vue.http.post("login", { password: password }).then(response => {
      let user = response.body;
      Vue.store.commit("setUserData", user);
      if ("MANAGER" === user.role) {
        Vue.router.push({ name: "manage" });
      } else if ("USER" === user.role) {
        Vue.router.push({ name: "use" });
      }
    }, reject);
  },
  logout() {
    return Vue.http.post("logout").then(() => {
      Vue.store.dispatch("reset");
      Vue.router.push({ name: "sign-in" });
    }, reject);
  },
  register(registerData) {
    return Vue.http.post("register", registerData).then(response => {
      Vue.store.commit("setUserData", response.body);
      Vue.router.push({ name: "home" });
    }, reject);
  }
};

export default SignService;
