import Vue from "vue";
import reject from "@/utils/reject";

let commit = response => {
  let world = response.body;
  Vue.store.commit("setWorld", world);
  return world;
};

let WorldsService = {
  get(id) {
    return Vue.http.get("worlds/" + id).then(commit, reject);
  },
  create() {
    return Vue.http.post("worlds").catch(reject);
  },
  delete(id) {
    return Vue.modal
      .msgBoxConfirm("Are you sure you want to delete the world?", {
        title: "We need your confirmation"
      })
      .then(ok => {
        if (ok) {
          return Vue.http.delete("worlds/" + id).catch(reject);
        } else {
          return Promise.reject("cancelled");
        }
      });
  },
  saveVehicles() {
    let world = Vue.store.getters.world;
    return Vue.http
      .post("worlds/" + world.id + "/vehicles", world.vehicles)
      .then(commit, reject);
  },
  addVehicle(vehicle) {
    let world = Vue.store.getters.world;
    world.vehicles.push(vehicle);
    return WorldsService.saveVehicles();
  },
  removeVehicle(vehicle) {
    return Vue.modal
      .msgBoxConfirm(
        `Are you sure you want to unassign the vehicle ${
          vehicle.name
        } from the world?`,
        { title: "We need your confirmation" }
      )
      .then(ok => {
        if (ok) {
          let world = Vue.store.getters.world;
          world.vehicles = world.vehicles.filter(v => v.id !== vehicle.id);
          return WorldsService.saveVehicles();
        } else {
          return Promise.reject("cancelled");
        }
      });
  },
  addStop(cell) {
    let world = Vue.store.getters.world;
    return Vue.http
      .post("worlds/" + world.id + "/stops", cell)
      .then(commit, reject);
  },
  addBuilding(cell) {
    let world = Vue.store.getters.world;
    return Vue.http
      .post("worlds/" + world.id + "/buildings", cell)
      .then(commit, reject);
  },
  clear(cell) {
    return Vue.modal
      .msgBoxConfirm(
        `Are you sure you want to destroy the spot ${cell.name}?`,
        { title: "We need your confirmation" }
      )
      .then(ok => {
        if (ok) {
          let world = Vue.store.getters.world;
          return Vue.http
            .post("worlds/" + world.id + "/clear", cell)
            .then(commit, reject);
        } else {
          return Promise.reject("cancelled");
        }
      });
  }
};

export default WorldsService;
