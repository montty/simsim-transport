import Vue from "vue";
import reject from "@/utils/reject";

let SettingsService = {
  save() {
    return Vue.http
      .post("settings", Vue.store.getters.user.settings)
      .then(response => {
        Vue.store.commit("setUserData", response.body);
      }, reject);
  },
  selectWorld(world) {
    Vue.store.getters.user.settings.worlds.push(world);
    return SettingsService.save().then(() => {
      Vue.router.push({ name: "settings.main" });
    });
  },
  removeWorld(world) {
    return Vue.modal
      .msgBoxConfirm(
        `Are you sure you want to remove ${world.name} from your preferences?`,
        {
          title: "We need your confirmation"
        }
      )
      .then(ok => {
        if (ok) {
          Vue.store.getters.user.settings.worlds = Vue.store.getters.user.settings.worlds.filter(
            v => v.id != world.id
          );
          return SettingsService.save().then(() => {
            Vue.router.push({ name: "settings.main" });
          });
        } else {
          return Promise.reject("cancelled");
        }
      });
  }
};

export default SettingsService;
