import Vue from "vue";
import reject from "@/utils/reject";

let commit = response => {
  let vehicle = response.body;
  Vue.store.commit("setVehicle", vehicle);
  return vehicle;
};

let VehiclesService = {
  get(id) {
    return Vue.http.get("vehicles/" + id).then(commit, reject);
  },
  create() {
    return Vue.http.post("vehicles").catch(reject);
  },
  delete(id) {
    return Vue.modal
      .msgBoxConfirm("Are you sure you want to destroy the vehicle?", {
        title: "We need your confirmation"
      })
      .then(ok => {
        if (ok) {
          return Vue.http.delete("vehicles/" + id).catch(reject);
        } else {
          return Promise.reject("cancelled");
        }
      });
  }
};

export default VehiclesService;
