import Vue from "vue";
import Vuex from "vuex";
import businessLogic from "./businessLogic";
import SignService from "./services/signService";
import SettingsService from "./services/settingsService";
import WorldsService from "./services/worldsService";
import VehiclesService from "./services/vehiclesService";

Vue.use(Vuex);

let initialState = () => {
  return {
    user: null,
    world: null,
    vehicle: null,
    route: {
      way: {
        start: null,
        end: null
      },
      routes: []
    }
  };
};

export default new Vuex.Store({
  state: initialState(),
  mutations: {
    setUserData(state, user) {
      if (user && user.settings && user.settings.waytypeChoices) {
        let waytypeChoices = user.settings.waytypeChoices;
        Object.keys(user.settings.waytypeChoices).forEach(
          v =>
            (waytypeChoices[v].name =
              businessLogic.model.app.vehicles.wayTypeDisplayNames[v])
        );
      }
      state.user = user;
    },
    setWorld(state, world) {
      state.world = world;
    },
    setVehicle(state, vehicle) {
      state.vehicle = vehicle;
    },
    setRoute(state, route) {
      state.route = route;
    }
  },
  getters: {
    user: state => state.user,
    world: state => state.world,
    vehicle: state => state.vehicle,
    route: state => state.route
  },
  actions: {
    getUserData() {
      return SignService.getUserData();
    },
    login(store, login) {
      return SignService.login(login);
    },
    password(store, password) {
      return SignService.password(password);
    },
    logout() {
      return SignService.logout();
    },
    register(store, registerData) {
      return SignService.register(registerData);
    },
    saveSettings() {
      return SettingsService.save();
    },
    selectWorld(store, world) {
      return SettingsService.selectWorld(world);
    },
    removeWorldFromSettings(store, world) {
      return SettingsService.removeWorld(world);
    },
    getWorld(store, id) {
      return WorldsService.get(id);
    },
    createWorld() {
      return WorldsService.create();
    },
    deleteWorld(store, id) {
      return WorldsService.delete(id);
    },
    addVehicle(store, vehicle) {
      return WorldsService.addVehicle(vehicle);
    },
    removeVehicleFromWorld(store, vehicle) {
      return WorldsService.removeVehicle(vehicle);
    },
    addStopToWorld(store, cell) {
      return WorldsService.addStop(cell);
    },
    addBuildingToWorld(store, cell) {
      return WorldsService.addBuilding(cell);
    },
    removeStopOrBuildingFromWorld(store, cell) {
      return WorldsService.clear(cell);
    },
    getVehicle(store, id) {
      return VehiclesService.get(id);
    },
    createVehicle() {
      return VehiclesService.create();
    },
    deleteVehicle(store, id) {
      return VehiclesService.delete(id);
    },
    reset(store) {
      Object.assign(store.state, initialState());
    }
  }
});
