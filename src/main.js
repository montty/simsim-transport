import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import BootstrapVue from "bootstrap-vue";
import VueResource from "vue-resource";
import businessLogic from "./businessLogic";
import "./assets/global.scss";
import "format-unicorn";

Vue.use(VueResource);
Vue.use(BootstrapVue);

Vue.config.productionTip = false;

Vue.http.options.root = businessLogic.model.app.serverUrl;
Vue.http.options.credentials = true;

Vue.http.interceptors.push(function() {
  let id = Math.random(),
    app = businessLogic.model.app;
  app.loading.push(id);
  return function() {
    app.loading = app.loading.filter(v => v !== id);
  };
});

Vue.mixin({
  beforeCreate() {
    Vue.util.defineReactive(this, "app", businessLogic.model.app);
  }
});

Vue.router = router;
Vue.store = store;

let vue = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

Vue.root = vue.$root;
Vue.modal = vue.$bvModal;