import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "",
      name: "home",
      components: {
        default: Home,
        topmost: () => import("./components/Hello.vue")
      },
      meta: {
        abstract: { MANAGER: "manage", USER: "use" },
        roles: ["MANAGER", "USER"]
      },
      children: [
        {
          path: "manage",
          name: "manage",
          meta: {
            abstract: { MANAGER: "worlds" },
            roles: ["MANAGER"]
          },
          component: () => import("./views/manage/Manage.vue"),
          children: [
            {
              path: "worlds",
              name: "worlds",
              meta: {
                roles: ["MANAGER"]
              },
              component: () => import("./views/manage/Worlds.vue")
            },
            {
              path: "worlds/:id",
              name: "world",
              meta: {
                roles: ["MANAGER"]
              },
              props: true,
              component: () => import("./views/manage/World.vue")
            },
            {
              path: "worlds/:id/vehicles",
              name: "world.vehicles",
              meta: {
                roles: ["MANAGER"]
              },
              props: true,
              component: () => import("./views/manage/world/Vehicles.vue")
            },
            {
              path: "worlds/:id/stops",
              name: "world.stops",
              meta: {
                roles: ["MANAGER"]
              },
              props: true,
              component: () => import("./views/manage/World.vue")
            },
            {
              path: "vehicles",
              name: "vehicles",
              meta: {
                roles: ["MANAGER"]
              },
              component: () => import("./views/manage/Vehicles.vue")
            },
            {
              path: "vehicles/:id",
              name: "vehicle",
              meta: {
                roles: ["MANAGER"]
              },
              props: true,
              component: () => import("./views/manage/Vehicle.vue")
            }
          ]
        },
        {
          path: "use",
          name: "use",
          meta: {
            abstract: { USER: "settings" },
            roles: ["USER"]
          },
          component: () => import("./views/use/Use.vue"),
          children: [
            {
              path: "settings",
              name: "settings",
              meta: {
                abstract: { USER: "settings.main" },
                roles: ["USER"]
              },
              component: () => import("./views/use/Settings.vue"),
              children: [
                {
                  path: "main",
                  name: "settings.main",
                  meta: {
                    roles: ["USER"]
                  },
                  components: {
                    worlds: () => import("./views/use/settings/Main.vue")
                  }
                },
                {
                  path: "select-world",
                  name: "settings.select-world",
                  meta: {
                    roles: ["USER"]
                  },
                  components: {
                    worlds: () => import("./views/use/settings/Worlds.vue")
                  }
                }
              ]
            },
            {
              path: "route-search",
              name: "route-search",
              meta: {
                abstract: { USER: "route-search.main" },
                roles: ["USER"]
              },
              component: () => import("./views/use/Routes.vue"),
              children: [
                {
                  path: "main",
                  name: "route-search.main",
                  meta: {
                    roles: ["USER"]
                  },
                  components: {
                    routes: () => import("./views/use/routes/Main.vue")
                  }
                },
                {
                  path: "worlds/:id",
                  name: "route-search.world",
                  meta: {
                    roles: ["USER"]
                  },
                  props: {routes: true},
                  components: {
                    routes: () => import("./views/use/routes/World.vue")
                  }
                },
                {
                  path: "vehicles/:id",
                  name: "route-search.vehicle",
                  meta: {
                    roles: ["USER"]
                  },
                  props: {routes: true},
                  components: {
                    routes: () => import("./views/use/routes/Vehicle.vue")
                  }
                }
              ]
            }
          ]
        }
      ]
    },
    {
      path: "/sign-in",
      components: {
        default: () => import("./views/Signin.vue"),
        topmost: () => import("./components/Signup.vue")
      },
      meta: {
        bodyClass: "enter",
        abstract: { "": "sign-in" }
      },
      children: [
        {
          name: "sign-in",
          path: "",
          components: {
            default: () => import("./views/LoginForm.vue")
          }
        },
        {
          name: "password-form",
          path: "next",
          components: {
            default: () => import("./views/PasswordForm.vue")
          }
        }
      ]
    },
    {
      path: "/sign-up",
      components: {
        default: () => import("./views/Signup.vue"),
        topmost: () => import("./components/Signin.vue")
      },
      meta: {
        bodyClass: "enter",
        abstract: { "": "sign-up" }
      },
      children: [
        {
          name: "sign-up",
          path: "",
          component: () => import("./views/RegisterForm.vue")
        }
      ]
    },
    {
      path: "/error",
      name: "error",
      component: () => import("./views/Error.vue"),
      meta: {
        roles: []
      }
    }
  ]
});

router.beforeResolve((to, from, next) => {
  let user = Vue.store.getters.user;
  const protectedRoute =
    (to.meta.roles && to.meta.roles.length) ||
    to.matched.some(route => route.meta.roles && route.meta.roles.length);
  if (protectedRoute) {
    if (!user) {
      Vue.store.dispatch("getUserData").then(
        user => {
          if (user) {
            if (to.meta.abstract) {
              next({ name: to.meta.abstract[user.role] });
            } else {
              next();
            }
          } else {
            next({ name: "sign-in" });
          }
        },
        () => {
          next({ name: "sign-in" });
        }
      );
      // next(false);
    } else if (to.meta.abstract) {
      next({ name: to.meta.abstract[user.role] });
    } else if (
      to.meta.roles &&
      to.meta.roles.length &&
      !to.meta.roles.includes(user.role)
    ) {
      next({ name: "error", query: { msg: "Access Denied" } });
    } else {
      next();
    }
  } else {
    next();
  }
});

router.afterEach(to => {
  let routeWithBodyClass = to.matched.find(route => route.meta.bodyClass);
  document.body.className =
    (routeWithBodyClass && routeWithBodyClass.meta.bodyClass) || "";
});

export default router;
