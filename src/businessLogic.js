let businessLogic = {
  model: { app: {
    serverUrl: 'http://spbws-prc389.t-systems.ru:8109',
    loading: [],
    worlds: {
      // endpoint: 'worlds'
    },
    vehicles: {
      // endpoint: 'vehicles',
      wayTypeDisplayNames: {
        AIR: 'Air. Like a queen in chess, it can travel anywhere.',
        WATER: 'Water. Restricted to only water cells.',
        UNDERGROUND: 'Underground. Restricted to only subway cells.',
        GROUND: 'Ground. Avoids trees, buildings, and water. Like a root in chess.'
      }
    }  
  } 
  },
};

export default businessLogic;
