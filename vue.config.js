module.exports = {
  devServer: {
    watchOptions: {
      aggregateTimeout: 1000,
      poll: 2000
    }
  },
  pluginOptions: {
  },
  configureWebpack:
    { output: { filename: '[name].js', chunkFilename: 'js/[name].app.js', publicPath: '/' } }
}
